/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.bookscrud.services;

import in.ashish29agre.bookscrud.config.EntityManagerService;
import in.ashish29agre.bookscrud.model.Book;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.jvnet.hk2.annotations.Service;

/**
 *
 * @author Ashish
 */
@EJB
@Stateless
@Service
@Singleton
@Path("books")
public class BookFacadeREST extends AbstractFacade<Book> {
    @PersistenceContext(unitName = "in.ashish29agre_BooksCRUD_war_1.0PU")
    private EntityManager em;
    
    @Inject
    private EntityManagerService eMangerService;

    public BookFacadeREST() {
        super(Book.class);
    }

    @Path("/create")
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Book entity) {
        System.out.println("Book: " + entity.getName());
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Long id, Book entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Long id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Book find(@PathParam("id") Long id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Book> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Book> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return eMangerService.getEntityManager();
    }
    
}
