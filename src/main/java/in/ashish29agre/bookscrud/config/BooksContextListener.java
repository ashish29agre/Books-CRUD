/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.bookscrud.config;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Web application lifecycle listener.
 *
 * @author Ashish
 */
@WebListener
public class BooksContextListener implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(BooksContextListener.class.getSimpleName());
    
    private static final String PERSISTENT_UNIT_NAME = "in.ashish29agre_BooksCRUD_war_1.0PU";
    private static EntityManagerFactory emf;
    private static EntityManager eManager;
    
    @Inject
    private EntityManagerService entityManagerService;

    public BooksContextListener() {
    }
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.log(Level.INFO, "Context Initialized");
//        initEntityManager();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.log(Level.INFO, "Context Destroyed");
//        destroyEntityManager();
    }

//    private static void initEntityManager() {
//        emf = Persistence.createEntityManagerFactory(PERSISTENT_UNIT_NAME);
//        eManager = emf.createEntityManager();
//        logger.info("Initialized Entity Manager");
//    }
//
//    private static void destroyEntityManager() {
//        if (emf != null && eManager != null) {
//            emf.close();
//        }
//    }
//
//    public static EntityManager getEntityManager() {
//        if (eManager == null) {
//            logger.log(Level.SEVERE, "Entity Manager wall found null");
//            initEntityManager();
//        }
//        return eManager;
//    }
}
